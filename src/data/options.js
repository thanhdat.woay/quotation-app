import { defineType } from '.'
const options = [
  {
    name: 'as-1',
    title: 'Vòng quay may mắn',
    parent: 'question-1',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-2',
    title: 'Mở quà',
    parent: 'question-1',
    cost: 1002,
    isActive: () => true
  },
  {
    name: 'as-3',
    title: 'Có',
    parent: 'question-2',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-4',
    title: 'Không',
    parent: 'question-2',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-5',
    title: 'Mã chơi được in trên bao bì/sản phẩm',
    parent: 'question-3',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-6',
    title: 'Tích hợp với hệ thống xác thực đã mua hàng (vd: POS/CRM)',
    parent: 'question-3',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-7',
    title: 'Khách điền thông tin & nhập mã hóa đơn + ảnh hóa đơn',
    parent: 'question-3',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-8',
    parent: 'question-4',
    title: 'Có',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-9',
    parent: 'question-4',
    title: 'Không',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-10',
    parent: 'question-5',
    title: 'SMS OTP',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-11',
    parent: 'question-5',
    title: 'Form thông tin',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-12',
    parent: 'question-5',
    title: 'Zalo OA',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-13',
    parent: 'question-5',
    title: 'Mobile App',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-14',
    parent: 'question-5',
    title: 'Hệ thống Loyalty riêng',
    cost: 1001,
    isActive: () => true
  },
  {
    name: 'as-15',
    parent: 'question-6',
    cost: 1001,
    title: 'Có',
    isActive: () => true
  },
  {
    name: 'as-16',
    parent: 'question-6',
    cost: 1001,
    title: 'Không',
    isActive: () => true
  },
  {
    name: 'as-17',
    parent: 'question-7',
    cost: 1001,
    title: 'Urbox',
    isActive: () => true
  },
  {
    name: 'as-19',
    parent: 'question-7',
    cost: 1001,
    title: 'Vihat',
    isActive: () => true
  },
  {
    name: 'as-20',
    parent: 'question-7',
    cost: 1001,
    title: 'Prime',
    isActive: () => true
  },
  {
    name: 'as-21',
    parent: 'question-7',
    cost: 1001,
    title: 'Hệ thống khác',
    isActive: () => true
  },
  {
    name: 'as-21',
    parent: 'question-8',
    cost: 1001,
    title: 'Quà vật lý',
    isActive: () => true
  },
  {
    name: 'as-22',
    parent: 'question-8',
    cost: 1001,
    title: 'E-voucher',
    isActive: () => true
  },
  {
    name: 'as-23',
    parent: 'question-8',
    cost: 1001,
    title: 'Topup/Thẻ cào',
    isActive: () => true
  },
  {
    name: 'as-24',
    parent: 'question-8',
    cost: 1001,
    title: 'E-voucher từ các Merchant khác(Vd: Phúc long, Starbuck,..)',
    isActive: (userAnswers) => {
      const questionName = 'question-6'
      const ans = 'as-15'

      if (!userAnswers) {
        return false
      }

      return userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[defineType.single]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'as-25',
    parent: 'question-9',
    cost: 1001,
    isActive: () => true,
    title: 'Có'
  },
  {
    name: 'as-26',
    parent: 'question-9',
    cost: 1001,
    isActive: () => true,
    title: 'Không'
  },
  {
    name: 'as-29',
    parent: 'question-10',
    cost: 1001,
    isActive: () => true,
    title: 'Có'
  },
  {
    name: 'as-30',
    parent: 'question-10',
    cost: 1001,
    isActive: () => true,
    title: 'Không'
  },
  {
    name: 'as-31',
    parent: 'question-11',
    cost: 1001,
    isActive: () => true,
    title: 'Tư vấn'
  },
  {
    name: 'as-32',
    parent: 'question-11',
    cost: 1001,
    isActive: () => true,
    title: 'Thiết kế'
  },
  {
    name: 'as-33',
    parent: 'question-11',
    cost: 1001,
    isActive: () => true,
    title: 'Lập trình'
  },
  {
    name: 'as-34',
    parent: 'question-11',
    cost: 1001,
    isActive: () => true,
    title: 'Tối ưu Game'
  },
  {
    name: 'as-35',
    parent: 'question-11',
    cost: 1001,
    isActive: () => true,
    title: 'Vận hành(báo cáo)'
  },
  {
    name: 'as-36',
    parent: 'question-12',
    cost: 1001,
    isActive: () => true,
    title: 'Nghiên cứu ngành hàng'
  },
  {
    name: 'as-37',
    parent: 'question-12',
    cost: 1001,
    isActive: () => true,
    title: 'Đề xuất ý tưởng'
  },
  {
    name: 'as-38',
    parent: 'question-12',
    cost: 1001,
    isActive: () => true,
    title: 'Kinh nghiệm vận hành'
  },
  {
    name: 'as-39',
    parent: 'question-12',
    cost: 1001,
    isActive: () => true,
    title: 'Thể lệ & đăng ký TnC'
  },
  {
    name: 'as-40',
    parent: 'question-13',
    cost: 1001,
    isActive: () => true,
    title: 'Các thành phần trong game, vd: Viền, Mâm, Mảnh VQMM'
  },
  {
    name: 'as-41',
    parent: 'question-13',
    cost: 1001,
    isActive: () => true,
    title: 'Landing Page'
  },
  {
    name: 'as-42',
    parent: 'question-14',
    cost: 1001,
    isActive: () => true,
    title: 'Bạn đã có sẵn LDP, chỉ cần Woay tích hợp vào LDP chỉ định'
  },
  {
    name: 'as-43',
    parent: 'question-14',
    cost: 1001,
    isActive: () => true,
    title: 'Lập trình LDP'
  },
  {
    name: 'as-44',
    parent: 'question-15',
    cost: 1001,
    isActive: () => true,
    title: '200 người/cùng thời điểm'
  },
  {
    name: 'as-45',
    parent: 'question-15',
    cost: 1001,
    isActive: () => true,
    title: '500 người/cùng thời điểm'
  },
  {
    name: 'as-46',
    parent: 'question-15',
    cost: 1001,
    isActive: () => true,
    title: '2.000 người/cùng thời điểm'
  },
  {
    name: 'as-47',
    parent: 'question-15',
    cost: 1001,
    isActive: () => true,
    title: '10.000 người/cùng thời điểm'
  },
  {
    name: 'as-48',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '< 01 tháng'
  },
  {
    name: 'as-49',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '02 tháng'
  },
  {
    name: 'as-50',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '03 tháng'
  },
  {
    name: 'as-51',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '04 tháng'
  },
  {
    name: 'as-52',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '05 tháng'
  },
  {
    name: 'as-53',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '06 tháng'
  },
  {
    name: 'as-54',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '07 tháng'
  },
  {
    name: 'as-55',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '08 tháng'
  },
  {
    name: 'as-56',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '09 tháng'
  },
  {
    name: 'as-57',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '10 tháng'
  },
  {
    name: 'as-58',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '11 tháng'
  },
  {
    name: 'as-59',
    parent: 'question-16',
    isActive: () => true,
    cost: 1001,
    title: '12 tháng'
  },
  {
    name: 'as-60',
    parent: 'question-17',
    cost: 1001,
    isActive: () => true,
    title: 'Website'
  },
  {
    name: 'as-61',
    parent: 'question-17',
    cost: 1001,
    title: 'Mobile App',
    isActive: function (userAnswers) {
      const questionName = 'question-6'
      const ans = 'as-15'

      if (!userAnswers) {
        return true
      }

      return !userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[defineType.single]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'as-62',
    parent: 'question-17',
    cost: 1001,
    title: 'Zalo OA',
    isActive: function (userAnswers) {
      const questionName = 'question-6'
      const ans = 'as-15'

      if (!userAnswers) {
        return true
      }

      return !userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[defineType.single]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'as-63',
    parent: 'question-17',
    cost: 1001,
    title: 'Mini-App Zalo OA',
    isActive: function (userAnswers) {
      const questionName = 'question-6'
      const ans = 'as-15'

      if (!userAnswers) {
        return true
      }

      return !userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[defineType.single]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'as-64',
    parent: 'question-17',
    cost: 1001,
    isActive: () => true,
    title: 'Màn hình chỉ định tại Cửa Hàng'
  },
  {
    name: 'as-65',
    parent: 'question-17',
    cost: 1001,
    isActive: () => true,
    title: 'POS của đối tác/nhãn hàng'
  },
  {
    name: 'as-66',
    parent: 'question-23',
    isActive: () => true,
    cost: 1001,
    title: 'Increasing Penetration'
  },
  {
    name: 'as-67',
    parent: 'question-23',
    isActive: () => true,
    cost: 1001,
    title: 'Purchase more frequent'
  },
  {
    name: 'as-68',
    parent: 'question-23',
    isActive: () => true,
    cost: 1001,
    title: 'Purchase more volume'
  },
  {
    name: 'as-69',
    parent: 'question-23',
    isActive: () => true,
    cost: 1001,
    title: 'Trade up to higher product'
  }
]

export default options
