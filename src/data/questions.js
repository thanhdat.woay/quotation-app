import { defineType } from '.'

const questions = [
  {
    subtitle: '',
    title: 'Thể loại game',
    parent: 'type',
    name: 'question-1',
    type: defineType.single,
    isActive: () => true
  },
  {
    subtitle: '',
    title: 'Có yêu cầu người tiêu dùng mua hàng không?',
    parent: 'account',
    name: 'question-2',
    type: defineType.single,
    isActive: () => true
  },
  {
    name: 'question-3',
    subtitle: '',
    title: 'Xác thực đã mua hàng bằng cách nào?',
    type: defineType.single,
    parent: 'account',
    isActive: function (userAnswers) {
      const questionName = 'question-2'
      const ans = 'as-3'

      if (!userAnswers) {
        return false
      }

      return userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[this.type]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'question-4',
    subtitle: '',
    title: 'Có yêu cầu người tiêu dùng đăng nhập không?',
    type: defineType.single,
    parent: 'account',
    isActive: () => true
  },
  {
    name: 'question-5',
    subtitle: '',
    title: 'Hình thức đăng ký bằng cách nào?',
    type: defineType.single,
    parent: 'integration',
    isActive: function (userAnswers) {
      const questionName = 'question-4'
      const ans = 'as-8'
      if (!userAnswers) {
        return false
      }

      return userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[this.type]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'question-6',
    subtitle: '',
    title: 'Có đồng bộ dữ liệu với hệ thống sẵn có không?',
    type: defineType.single,
    parent: 'integration',
    isActive: () => true
  },
  {
    name: 'question-7',
    subtitle: '',
    title: 'Hệ thống bạn đang sử dụng có nằm trong đối tác của Woay không?',
    type: defineType.single,
    parent: 'integration',
    isActive: function (userAnswers) {
      const questionName = 'question-6'
      const ans = 'as-15'

      if (!userAnswers) {
        return false
      }

      return userAnswers.some(
        (item) =>
          item.question.name === questionName &&
          JSON.stringify(item[this.type]) === JSON.stringify(ans)
      )
    }
  },
  {
    name: 'question-8',
    subtitle: '',
    title: 'Phạm vi quà tặng dự kiến',
    type: defineType.multi,
    parent: 'gift',
    isActive: () => true
  },
  {
    name: 'question-9',
    title: 'Bạn đã có đối tác cung cấp quà giá tốt & tiện lợi chưa?',
    parent: 'gift',
    type: defineType.single,
    isActive: () => true
  },
  {
    name: 'question-10',
    title: 'Có gửi tin nhắn trúng thưởng sau khi tham gia không?',
    parent: 'gift',
    type: defineType.single,
    isActive: () => true
  },
  {
    name: 'question-11',
    title: 'Phạm vi công việc bạn cần Woay triển khai',
    parent: 'deployment',
    type: defineType.multi,
    isActive: () => true
  },
  {
    name: 'question-12',
    title: 'Hạng mục tư vấn bạn cần?',
    parent: 'deployment',
    type: defineType.multi,
    isActive: () => true
  },
  {
    name: 'question-13',
    title: 'Hạng mục thiết kế bạn cần?',
    parent: 'deployment',
    type: defineType.multi,
    isActive: () => true
  },
  {
    name: 'question-14',
    title: 'Hạng mục lập trình bạn cần?',
    parent: 'deployment',
    type: defineType.multi,
    isActive: () => true
  },
  {
    name: 'question-15',
    parent: 'distribution',
    title: 'Số lượng người chơi tham gia dự kiến 01 ngày',
    subtitle: 'Dữ liệu này nhằm để ước tính Hạ tầng (Server) chịu tải cần cấu hình bao nhiêu',
    type: defineType.single,
    tag:'select',
    isActive: () => true
  },
  {
    name: 'question-16',
    parent: 'distribution',
    title: 'Số tháng chạy chiến dịch',
    type: defineType.single,
    tag:'select',
    isActive: () => true
  },
  {
    name: 'question-17',
    parent: 'distribution',
    title: 'Kênh phân phối/tích hợp Game',
    type: defineType.single,
    isActive: () => true
  },
  {
    name: 'question-18',
    parent: 'contact',
    title: 'Họ tên',
    type: defineType.text,
    isActive: () => true
  },
  {
    name: 'question-19',
    parent: 'contact',
    title: 'Số điện thoại',
    type: defineType.phone,
    isActive: () => true
  },
  {
    name: 'question-20',
    parent: 'contact',
    title: 'Email',
    type: defineType.email,
    isActive: () => true
  },
  {
    name: 'question-21',
    parent: 'contact',
    title: 'Tên Agentcy (nếu có)',
    type: defineType.text,
    isEmpty: true,
    isActive: () => true
  },
  {
    name: 'question-22',
    parent: 'contact',
    title: 'Tên nhãn hàng',
    type: defineType.text,
    isActive: () => true
  },
  {
    name: 'question-23',
    parent: 'contact',
    title: 'Mục tiêu',
    type: defineType.single,
    isActive: () => true
  }
]

export default questions
