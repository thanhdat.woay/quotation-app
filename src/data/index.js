export const defineType = {
  text: 'text',
  single: 'single-choice',
  multi: 'multi-choice',
  email: 'email',
  phone: 'phone'
}

export const defineRegexValidate = {
  text: {
    value: /\S/,
    message: 'Bắt buộc'
  },
  email: {
    value:
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    message: 'Sai định dạng email. Ex: woay@gmail.com'
  },
  phone: {
    value: /^\d{10}$/,
    message: 'Sai định dạng số điện thoại. Ex: 0881231233'
  }
}
