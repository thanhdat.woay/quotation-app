const sections = [
  {
    title: 'Thể loại',
    subtitle: '',
    name: 'type',
    isActive: () => true
  },
  {
    title: 'Tài khoản',
    subtitle: '',
    name: 'account',
    isActive: () => true
  },
  {
    title:'Tích hợp',
    subtitle:'',
    name:'integration',
    isActive:() => true,
  },
  {
    title: 'Quà tặng',
    subtitle: '',
    name: 'gift',
    isActive: () => true
  },
  {
    title: 'Triển khai',
    subtitle: '',
    name: 'deployment',
    isActive: () => true
  },
  {
    title: 'Phân phối',
    subtitle: '',
    name: 'distribution',
    isActive: () => true
  },
  {
    title: 'Thông tin liên lạc',
    subtitle: '',
    name: 'contact',
    isActive: () => true
  }
]

export default sections
